This project is a polynomial calculator with graphical user interface. <br>
The calculator can perform the following operations: <br> <br>

-add and substract polynomials <br>
-integrate and differentiate polynomials <br>
-multiply and divide polynomials <br>