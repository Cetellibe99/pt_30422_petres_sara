package Controller;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import Model.Monomial;
import Model.PoliWithRemainder;
import Model.Polynomial;
import View.View;

/** @author Petres Sara
 * implementation of the methods used for the arithemtical operations
 * and the auxiliary methods which were neccessarry for the correct functioning of the GUI 
 * or the operational methods
**/

public class Operations {
	//all the  functions used for the implementation:
	/**it gets the string representaion of a monomial, for the output textfields @return a String**/
	public static String getMonoString(Monomial mono){ //returns the string form of a monomial (for GUI)
		String myString = new String();
		String coeff = String.valueOf(mono.coeff);
		String degree = String.valueOf(mono.degree);
		
		if(coeff.contains("-")) {
			myString = coeff + "x^" + degree;
		}
		else
			myString = "+"+coeff + "x^" + degree;
	
		return myString;
	} /**gets the string representaion of a polynomial, it applies getMonoString for each term, @return a String**/
	public static String getPoliString(Polynomial pol) { //returns the string form of a polynomial(for GUI)
		String myPoli = new String();
		for (Monomial mono : pol.MonoList) {
			myPoli += getMonoString(mono);
		}
		return myPoli ;
	}
/**checks if an input string is in monomial format, ex: -2x^3 or 2x^3**/
	public static Boolean checkMonoFormat(String s) { //checks if a string is in monomial format(for GUI)
		String temp=s;
		Pattern p = Pattern.compile("-?\\d+(?:.\\d+)?[a-zA-Z]\\^-?\\d+(?:.\\d+)?");
		if(temp.charAt(0)=='+')
		temp=temp.replace("+","");
		if(temp.charAt(0)=='-')
			temp=temp.replace("-","");
		if(temp.charAt(1)!='x')
			return false;
			
		Matcher mono = p.matcher(temp);
		if(mono.matches()) {
			return true;
		}
		return false;
	}
	 boolean isDouble(String str) {
	        try {
	            Double.parseDouble(str);
	            return true;
	        } catch (NumberFormatException e) {
	            return false;
	        }
	    }/**decides which polynomial starts with a higher degree**/
	static Polynomial bigger(Polynomial pol1, Polynomial pol2) { //decides which polynomial starts with a bigger degree
		
		if (pol1.MonoList.get(0).degree>pol2.MonoList.get(0).degree)
			return pol1;
		else if (pol1.MonoList.get(0).degree == pol2.MonoList.get(0).degree)
		{
			if (pol1.MonoList.size()>pol2.MonoList.size())
				return pol1;
			else
				return pol2;
		}
		else return pol2;		
	}
	/**rearrange the order of degrees in descending order, @return a Polynomial**/
	public static void rearrangePoly(Polynomial pol) { 
		Monomial tempi = new Monomial(0,0);
		Monomial tempj = new Monomial(0,0);
		
		for (int i=0; i<pol.MonoList.size();++i ) {
			for (int j=i+1; j<pol.MonoList.size(); ++j) {
				if (pol.MonoList.get(i).degree < pol.MonoList.get(j).degree)
				{
					tempi=pol.MonoList.get(i);
					tempj=pol.MonoList.get(j);
					pol.MonoList.set(i, tempj);
					pol.MonoList.set(j, tempi); //swap
				}
			}
		}
	}
	/**extends the polynomial with 0x^n monomials, (used for division):**/
	public static Polynomial extendPoli(Polynomial pol) { 				
		Monomial temp = new Monomial(0,0);
		int k=0;		
		for (int i=0; i<pol.MonoList.size()-1; ++i)
		{
			if (pol.MonoList.get(i).degree >pol.MonoList.get(i+1).degree+1)
			{
				k =  pol.MonoList.get(i).degree-1;
				do {					
					temp = new Monomial(0,k);					
					pol.MonoList.add(temp);
					--k;
				}
				while(k>pol.MonoList.get(i+1).degree);
			}
			k=0;
		}
		rearrangePoly(pol);
		return pol;
	}/**removes the 0 coefficient monomials from the beginning of the polynomial if it exists(used for division)**/
	public static Polynomial removeInitialZeros(Polynomial pol) { 
		int k=0;
		while(pol.MonoList.get(k).coeff == 0) {
			pol.MonoList.remove(k);
		}
		return pol;
	}/**removes all 0s from a polynomial, it is used for nice printing**/
	public static Polynomial removeAllZeros(Polynomial pol) {//removes all zero members from the polynomial(used for nice printing)
		for (Monomial mono: pol.MonoList)
		{
			if(mono.coeff ==0)
				pol.MonoList.remove(mono);
		}
		return pol;
	}/**implements the multiplication of a polynomial and a monomial, it is neccessary for the division algorithm**/
	public static Polynomial polytimesMono( Polynomial poli, Monomial mono ) { //used for division
		Polynomial timespol = new Polynomial();
		for (int i=0; i<poli.MonoList.size(); ++i) {
			timespol.MonoList.add(multiMono(poli.MonoList.get(i),mono));
		}
		return timespol;
	}
	/**the followings are atomic functions in the level of monomials,used
	for the respecting operations in the level of polynomials. Each of them @return a Monomial**/
	public static Monomial addMono(Monomial mon1, Monomial mon2) {
		Monomial sumMono = new Monomial(mon1.coeff+mon2.coeff, mon1.degree);
		return sumMono;			
	}
	public static Monomial subMono(Monomial mon1, Monomial mon2) {
		Monomial subMono = new Monomial(mon1.coeff-mon2.coeff, mon1.degree);
		return subMono;			
	}	
	public static Monomial multiMono(Monomial mon1, Monomial mon2) {
		Monomial multiMono = new Monomial(mon1.coeff*mon2.coeff,mon1.degree+mon2.degree);
		return multiMono;
	}	
	public static Monomial divideMono(Monomial mon1, Monomial mon2) {
		Monomial divideMono = new Monomial(mon1.coeff/mon2.coeff,mon1.degree-mon2.degree);
		return divideMono;
	}
	// ADDITION of 2 polynomials
	public static Polynomial addPoly (Polynomial pol1, Polynomial pol2) {
		int m=0; //index iterating over pol2
		int n=0; //index iterating over pol1
		Polynomial sumpol = new Polynomial();
		
		while(n<pol1.MonoList.size() && m<pol2.MonoList.size()) {
			if (pol1.MonoList.get(n).degree > pol2.MonoList.get(m).degree) {
				sumpol.MonoList.add(pol1.MonoList.get(n));
			
				++n;
			} 
			else if (pol1.MonoList.get(n).degree < pol2.MonoList.get(m).degree) {
					sumpol.MonoList.add(pol2.MonoList.get(m));
					++m;	
			}
			else { // case of equality
				sumpol.MonoList.add(addMono(pol1.MonoList.get(n),pol2.MonoList.get(m)));
				++m;
				++n;
			}
		} /**in case of one list is longer than the other, the remaining terms are added to the result list:**/
		if (n<pol1.MonoList.size()) {
			while(n<pol1.MonoList.size()) {
				sumpol.MonoList.add(pol1.MonoList.get(n));
				++n;
			}
		}
		if (m<pol2.MonoList.size()) {
			while(m<pol2.MonoList.size()) {
				sumpol.MonoList.add(pol2.MonoList.get(m));
				++m;
			}
		}
		return sumpol;
	}
  //SUBSTRACTION  of 2 polynomials		
	public static Polynomial subPoly (Polynomial pol1, Polynomial pol2) {
		int m=0; //index iterating over pol1
		int n=0; //index iterating over pol2
		Monomial sub = new Monomial(0,0);		
		Polynomial subpol = new Polynomial();
		
		while(n<pol1.MonoList.size() && m<pol2.MonoList.size()) {
			if (pol1.MonoList.get(n).degree > pol2.MonoList.get(m).degree) {
				subpol.MonoList.add(pol1.MonoList.get(n));				
				++n;
			} 
			else if (pol1.MonoList.get(n).degree < pol2.MonoList.get(m).degree) {
				sub = pol2.MonoList.get(m);
				sub.coeff = -sub.coeff;
					subpol.MonoList.add(sub);
					++m;
			}
			else { // case of equality
				subpol.MonoList.add(subMono(pol1.MonoList.get(n),pol2.MonoList.get(m)));
				++m;
				++n;
			}
		}/** adds the remaining monomials to the result list if one on the polynomials is longer than the other**/
		if (n<pol1.MonoList.size()) {
			while(n<pol1.MonoList.size()) {
				subpol.MonoList.add(pol1.MonoList.get(n));
				++n;
			}
		}
		if (m<pol2.MonoList.size()) {
			while(m<pol2.MonoList.size()) {
				sub = pol2.MonoList.get(m);
				sub.coeff = -sub.coeff;
				subpol.MonoList.add(sub);
				++m;
			}
		}
		return subpol;
	}
	//DIFFERENTIATION of a polynomial
	public static Polynomial derivatePoly(Polynomial pol) {
		
		for (int i=0; i<pol.MonoList.size(); ++i) {
			if (pol.MonoList.get(i).degree==0) {
				pol.MonoList.remove(i);
			}
		}
	
		for (Monomial mono: pol.MonoList) {
			mono.coeff = mono.coeff*mono.degree;
			mono.degree= mono.degree - 1;
		}
		return pol;
	} 
	// INTEGRATION of a polynomial
	public static Polynomial integratePoly(Polynomial pol) {
		for (Monomial mono: pol.MonoList) {
			mono.degree = mono.degree +1;
			mono.coeff = mono.coeff/mono.degree;			
		}
		return pol;
	}
	//MULTIPLICATION of two polynomials
	public static Polynomial multiPoli (Polynomial pol1, Polynomial pol2) // haha
	{
		Polynomial mulpoli = new Polynomial();		
		for (Monomial mono1: pol1.MonoList) {
		for(Monomial mono2: pol2.MonoList) {
			mulpoli.MonoList.add(multiMono(mono1,mono2));
		  }	
	   }		
		for (int i=0; i<mulpoli.MonoList.size(); ++i)
		{
			for (int j=i+1; j<mulpoli.MonoList.size(); ++j) {
				if (mulpoli.MonoList.get(i).degree == mulpoli.MonoList.get(j).degree)
				{
					mulpoli.MonoList.get(i).coeff += mulpoli.MonoList.get(j).coeff;
					mulpoli.MonoList.remove(j);					
				}
			}
		}
		rearrangePoly(mulpoli);
		return mulpoli;				
	}
	/**DIVISION of two polynomials, @return a PoliWithRemainder**/
	public static PoliWithRemainder dividePoli(Polynomial pol1, Polynomial pol2) { //pol1,pol2//mikor meghivjuk akkor nezzuk melyik nagyobb
		PoliWithRemainder result = new PoliWithRemainder();
		Polynomial up = new Polynomial();
		Polynomial down = new Polynomial();
		
		up = bigger(pol1,pol2); //divide the bigger rank polynomial with the lower rank
		if (up!=pol1)
			down = pol1;
		else
			down = pol2;
		
		pol1 = extendPoli(pol1); //adding the 0x^n monomials (n<biggest degree in the polynomial)
		pol2 = extendPoli(pol2);	// it is neccessary for a correct division algorithm			
		Polynomial res = new Polynomial();
		Polynomial temp = new Polynomial();
		Polynomial temp2 = new Polynomial();
		Monomial zero = new Monomial(0,0);
		Monomial rem=new Monomial(0,0);
		
		int resi; //indexes
		int upi;
		int tempi=0;
		int tempj=0;
		tempi = down.MonoList.size()-1;
			
		//1st step: detect mono-mono division
		if (up.MonoList.size()==1 && down.MonoList.size()==1) {
			res.MonoList.add(divideMono(up.MonoList.get(0),down.MonoList.get(0)));
			temp.MonoList.add(zero);
		}
		//2nd step: poly:mono division
		else if(up.MonoList.size()>1 && down.MonoList.size()==1){
			System.out.println("nice");
			for(Monomial mono: up.MonoList) {
				res.MonoList.add(divideMono(mono,down.MonoList.get(0)));
			}
		}
		else {
		//3rd step: algorithm for poli:poli division
		//initial steps	
		res.MonoList.add(divideMono(up.MonoList.get(tempj),down.MonoList.get(tempj)));		
		temp=multiPoli(res,down);	
		temp = subPoly(up,temp);			
		while(temp.MonoList.size()>down.MonoList.size())
		{
			temp.MonoList.remove(temp.MonoList.size()-1); //remove the last one
		}		
		temp.MonoList.remove(0); //remove the first which became null		
		++tempi;
		if (tempi<up.MonoList.size()) {
			temp.MonoList.add(up.MonoList.get(tempi));		
		}	
	//ciclu:
		
		do {
		res.MonoList.add(divideMono(temp.MonoList.get(tempj),down.MonoList.get(tempj)));
		
		temp2=temp;
		temp=polytimesMono(down, res.MonoList.get(res.MonoList.size()-1)); //multiplication with the last one
		temp = subPoly(temp2,temp);
				
		while(temp.MonoList.size()>down.MonoList.size())
		{
			temp.MonoList.remove(temp.MonoList.size()-1); //remove the last one
		}
		temp.MonoList.remove(0); //remove the first which became null
		
		++tempi;
		if (tempi<up.MonoList.size()) {
			temp.MonoList.add(up.MonoList.get(tempi));
		} 
		
		}
		while(divideMono(temp.MonoList.get(tempj),down.MonoList.get(tempj)).degree>=0);		
		} //end else bracket for mono or not mono detection

	result.MonoList=res.MonoList;
	result.remainder = temp.MonoList;
	return result;	
	}

	public static void main(String[] args) {
		//creating the GUI
		View view = new View();
		view.setVisible(true);	

	}
}
