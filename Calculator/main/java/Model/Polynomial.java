package Model;
import java.util.*;

/** @author Petres Sara
 * representation of the polynomial class with all of its atomic methods
**/
public class Polynomial {
	public List <Monomial> MonoList;
	
	public Polynomial() {
		MonoList= new ArrayList<>();
	}
	public Polynomial setElement(Monomial m) {
		this.MonoList.add(m);
		return this;
	}
	/** this  method uses the overriden version of the object.equals() built in method, which detect equal monomials
	 * it is used for unit testing	
	 * @param pol
	 * @return boolean
	 */
	public Boolean poliEquals(Polynomial pol) {
		if (this.MonoList.size() != pol.MonoList.size())
			return false;
		for(int i=0; i<this.MonoList.size(); i++) {
			if (this.MonoList.get(i).equals(pol.MonoList.get(i))==false)
				return false;
		}
		return true;
	}
	




	
	

}
