package Model;

/** @author Petres Sara
 * representation of the monomial class with all of its atomic methods
**/
public class Monomial {
	
	public double coeff;
	public int degree;
	
	public Monomial ( double coeff, int degree) {
		this.degree=degree;
		this.coeff=coeff;
	}
	
	public void setDegree(int degree) {
		this.degree= degree;
	}
	public void setCoeff(double coeff) {
		this.coeff = coeff;
	}
	public Monomial setMonomial(double coeff, int degree) {
		this.coeff = coeff;
		this.degree = degree;
		return this;
	}
	/** this overriden version of the object.equals() built in method detects if 2 Monomials are equal
	*It is used firstly for building a method to detect equal polynomials,
	*Furthermore, at unit testing when the expected polynomial should be compared with result polynomial
	@param o 
	@return boolean*/
    @Override
    public boolean equals(Object o) { 

        if (o == this) { 
            return true; 
        } 

        if (!(o instanceof Monomial)) { 
            return false; 
        } 
          
    
        Monomial c = (Monomial) o; 

        return Double.compare(coeff, c.coeff) == 0
                && Integer.compare(degree, c.degree) == 0; 
    } 


}
