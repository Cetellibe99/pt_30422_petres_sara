package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.*;

/** @author Petres Sara
 * representation of the PoliWithRemainder class with all of its atomic methods
*
*this is a specific result type for the division operation
*it contains a list instead for the result (MonoList) and a list for the rest(remainder)**/
public class PoliWithRemainder {
	public List <Monomial> MonoList;
	public List<Monomial> remainder;
	
	public PoliWithRemainder() {
		MonoList= new ArrayList<>();
		remainder = new ArrayList<>();
		
	}
	
	public PoliWithRemainder setElement(Monomial m) {
		this.MonoList.add(m);
		return this;
	}
	/** a method that check if two polynomials with remainders are equal
		// it uses an overridden version of the equals() method which is implemented in the Monomial class
		 * @param pol 
		 * @return boolean*/
	
	public Boolean poliWithRemainderEquals(PoliWithRemainder pol) {
		if ((this.MonoList.size()!=pol.MonoList.size()) || (this.remainder.size() != pol.remainder.size()))
			return false;
		
		for (int i=0; i<this.MonoList.size(); ++i) {
			if(this.MonoList.get(i).equals(pol.MonoList.get(i))==false)
				return false;
		}
		for (int i=0; i<this.remainder.size(); ++i) {
			if(this.remainder.get(i).equals(pol.remainder.get(i))==false)
				return false;
		}
				
		return true;
	}
	
	

}