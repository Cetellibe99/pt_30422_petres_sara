package View;

import java.util.ArrayList;

import java.io.*;
import java.util.List;
import java.lang.*;
import Controller.Operations;
import Model.Monomial;
import Model.PoliWithRemainder;
import Model.Polynomial;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.text.JTextComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** @author Petres Sara
 * Frame that represent the graphical user interface, describes the action performed when pressing one of the 8 buttons
 * and a method that detects if the input string is correct by using regular expressions
**/

public class View extends JFrame implements ActionListener  {
	/**implementation of the panels, label, dialog panels required for the frame**/
	JPanel p1 = new JPanel(); JPanel p2 = new JPanel(); JPanel p3 = new JPanel();
	JPanel p12 = new JPanel(); JPanel p23 = new JPanel(); JPanel dialog = new JPanel();
	JOptionPane jopt = new JOptionPane();
	JLabel err = new JLabel("Wrong input format, try  anx^n+..+a1x^1+a0x^0  form,  example: 2x^3+1x^0 ");
	JLabel emptytext = new JLabel("empty textfield :|");
	JLabel empty1, e1,eresult,erest, eb;
	
	private JTextField pol1; private JTextField pol2; //input text fields
	private JTextField result; private JTextField rest; //output text fields
	private JButton integralUp; private JButton derivativeUp; //action buttons
	private JButton integralDown; private JButton derivativeDown; //action buttons
	private JButton addpol,subpol,multipol,dividepol; //action buttons
	 
	 public View (){ 
	/**CREATING panels, buttons for the required GUI**/
		 Font myFont = new Font("Serif", Font.PLAIN, 20); Font myFont2 = new Font("Serif", Font.PLAIN, 30);		 
		 p1.setLayout(new GridLayout(2, 3,10,10)); p2.setLayout(new GridLayout(3,4,10,10)); 
		 p3.setLayout(new GridLayout(2,2,10,10)); p12.setLayout(new GridLayout(3,1));
		 p23.setLayout(new GridLayout(3,1));
		 
		 pol1 = new JTextField(); pol2 = new JTextField(); result = new JTextField(); rest = new JTextField();

	 /**setting text to the buttons**/
		 integralUp = new JButton("Integral");
		 derivativeUp = new JButton("Derivative");
		 integralDown = new JButton("Integral");
		 derivativeDown = new JButton("Derivative");
		 integralUp.setFont(myFont); derivativeUp.setFont(myFont); 
		 integralDown.setFont(myFont); derivativeDown.setFont(myFont);
		 
		 addpol = new JButton("+"); subpol = new JButton("-"); multipol = new JButton("*"); dividepol = new JButton("/");		 
		 addpol.setFont(myFont2); subpol.setFont(myFont2); multipol.setFont(myFont2); dividepol.setFont(myFont2);

	/**auxiliary labels, used for filling the grid format in the wanted way**/
		 empty1 = new JLabel("             "); 	 eb = new JLabel(" ");  e1 = new JLabel("                "); 
		 eresult = new JLabel("  Result      ");  eresult.setFont(myFont);
		 erest = new JLabel("  Remainder      "); erest.setFont(myFont);
		 err.setFont(myFont); emptytext.setFont(myFont);
		 
	/**connecting the panels with the textfields and the buttons**/		 		 		
		 p1.add(pol1); p1.add(integralUp); p1.add(derivativeUp);
		 p1.add(pol2); p1.add(integralDown); p1.add(derivativeDown);		 
		 p2.add(addpol); p2.add(subpol); p2.add(multipol);
		 p2.add(empty1); p2.add(dividepol);		 		 		 
		 p3.add(result); p3.add(rest); p3.add(eresult); p3.add(erest); 	 		 
		 p12.add(empty1); p23.add(empty1);
		 
	/**ARRANGEMENT of the panels**/
		 add(p1, "North"); add(p12, "East"); add(p2, "West");
		 add(p23, "Center"); add(p3, "South");		 
		 setVisible(true); this.setSize(700,300);

	/**ACTION listener for each button*/
		 integralUp.addActionListener(this); integralDown.addActionListener(this);
		 derivativeUp.addActionListener(this); derivativeDown.addActionListener(this);
		 addpol.addActionListener(this); subpol.addActionListener(this);
		 multipol.addActionListener(this); dividepol.addActionListener(this);		 
	 }
	@Override
	public void actionPerformed(ActionEvent e) {		
		//initialization			
		Polynomial polOne = new Polynomial();
		Polynomial polTwo = new Polynomial();
		Polynomial myPoli = new Polynomial(); // for addition, substraction, multiplication
		Polynomial myRest = new Polynomial(); //rest for division
		PoliWithRemainder divPoli = new PoliWithRemainder(); //result for division
		String temp1 = pol1.getText();
		String temp2 = pol2.getText();
		double d1,d2; Integer i1,i2; String aux= new String();

	/**REGEX for splitting the input text into coeffiect and degree terms**/
		String[] coef1 = temp1.split("x\\^\\d+\\+?"); //coeffcients
		String[] degre1 = temp1.split("\\+?\\d+x\\^"); //degrees 
		String[] coef2 = temp2.split("x\\^\\d+\\+?"); //coeffcients
		String[] degre2 = temp2.split("\\+?\\d+x\\^"); //degrees 			
		Monomial monos1[] = new Monomial[coef1.length];
		Monomial monos2[] = new Monomial[coef2.length];
				
		//INTEGRATE UP		
		 if (e.getSource()==integralUp) {			 
			 /**checks polynomial validity with checkPoliFormat(string s), @return Boolean**/
			 if(checkPoliFormat(pol1.getText())==false) {
				 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
			 }
			 else if(pol1.getText().equals("")) {
				 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
			 }
			 else
			 {			 
			 for(int k=0, j=1; k<coef1.length; ++k, ++j)

		        { /**CONVERTING the splitted coeffiecient string terms to double **/
				 try {
		        	d1 = Double.parseDouble(coef1[k].trim());
				 }
				 catch (NumberFormatException e1) { //if the number is negative, it throws an exception, so we have to catch that exception and give a sign to the result number
				 
					d1 = -Double.parseDouble(coef1[k].trim());
				 }
				 				 
				try { /**CONVERTING the splitted degree string terms into integer format, @throw NumberFormatException**/
		        	 i1=Integer.parseInt(degre1[j].trim());
				}
				catch(NumberFormatException e1) {
					  aux = degre1[j].trim();
		    		    aux=aux.substring(0, aux.length() - 1);
		    		    i1=Integer.parseInt(aux);
				}	//adding to the result monomial vector			
				 monos1[k] = new Monomial(d1,i1);			
		         }
			 
			 for (int k=0; k<monos1.length; ++k)
					 polOne.MonoList.add(monos1[k]); 
			 //operation performed respecting to the button pressed
			polOne=Operations.integratePoly(polOne);
			result.setText(Operations.getPoliString(polOne));
			 }
		}		 
		 //DERIVATIVE UP		
		 else if (e.getSource()==derivativeUp) {
			 if(checkPoliFormat(pol1.getText())==false) {
				 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
			 }
			 else if(pol1.getText().equals("")) {
				 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
			 }

			 else
			 {/**parsing the coefficient string to a double, and the degree string to an integer,@throw NumberFormatException**/	 
			 for(int k=0, j=1; k<coef1.length; ++k, ++j)
		        {
				 try {
					 d1 = Double.parseDouble(coef1[k].trim());
				 }
				 catch(NumberFormatException e1) {
					 d1 = -Double.parseDouble(coef1[k].trim());
				 }		        	
		        	 try { 
			        	 i1=Integer.parseInt(degre1[j].trim());
					}
					catch(NumberFormatException e1) {
						  aux = degre1[j].trim();
			    		    aux=aux.substring(0, aux.length() - 1);
			    		    i1=Integer.parseInt(aux);
					}		        	 		        	 
			    	 monos1[k] = new Monomial(d1,i1);
		         }			 
			 for (int k=0; k<monos1.length; ++k)
					 polOne.MonoList.add(monos1[k]); 
			 
			polOne=Operations.derivatePoly(polOne);
			result.setText(Operations.getPoliString(polOne));
			 }
		}	 
		 //INTEGRAL DOWN	 
		 else if (e.getSource()==integralDown) {
			 if(checkPoliFormat(pol2.getText())==false) {
				 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
			 }
			 else if(pol2.getText().equals("")) {
				 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
			 }

			 else
			 {	/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 			 
		 for(int k=0, j=1; k<coef2.length; ++k, ++j)
	        {
			 try {
	        	d2 = Double.parseDouble(coef2[k].trim());
			 }
			 catch(NumberFormatException e1) {
				 d2 = -Double.parseDouble(coef2[k].trim());
			 }	        	 
	        		try { 
			        	 i2=Integer.parseInt(degre2[j].trim());
					}
					catch(NumberFormatException e1) {
						aux = degre2[j].trim();
		    		    aux=aux.substring(0, aux.length() - 1);
		    		    i2=Integer.parseInt(aux);
					}	        	 
		    	 monos2[k] = new Monomial(d2,i2);
	         }		 		 
		 for (int k=0; k<monos2.length; ++k)
				 polTwo.MonoList.add(monos2[k]); 		 		
		polTwo=Operations.integratePoly(polTwo);		 
		result.setText(Operations.getPoliString(polTwo));
			 }
	}
		 //DERIVATIVE DOWN		 
		 else if (e.getSource()==derivativeDown) {
			 if(checkPoliFormat(pol2.getText())==false) {
				 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
			 }
			 else if(pol2.getText().equals("")) {
				 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
			 }

			 else
			 {	/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 		 
			 for(int k=0, j=1; k<coef2.length; ++k, ++j)
		        {
				 try {
		        	d2 = Double.parseDouble(coef2[k].trim());
				 }
				catch(NumberFormatException e1) {
					d2 = -Double.parseDouble(coef2[k].trim());
				}				 
				 try {
		        	 i2=Integer.parseInt(degre2[j].trim());
				 }
				 catch(NumberFormatException e1) {
					 aux = degre2[j].trim();
		    		    aux=aux.substring(0, aux.length() - 1);
		    		    i2=Integer.parseInt(aux);
				 }
			    	 monos2[k] = new Monomial(d2,i2);
		         }			 
			 for (int k=0; k<monos2.length; ++k)
					 polTwo.MonoList.add(monos2[k]); 
			
			polTwo=Operations.derivatePoly(polTwo);			 
			result.setText(Operations.getPoliString(polTwo));
			 }
		}	
	//ADITION
	 else if (e.getSource()==addpol) {
		 if(checkPoliFormat(pol1.getText())==false || checkPoliFormat(pol2.getText())==false) {
			 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
		 }
		 else if(pol1.getText().equals("") || pol2.getText().equals("")) {
			 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
		 }

		 else
		 {		/**parsing the coefficient string into a double, and the degree string to an integer	,@throw NumberFormatException  **/
		 for(int k=0, j=1; k<coef1.length; ++k, ++j)
	        {
			 try {
	        	d1 = Double.parseDouble(coef1[k].trim());
			 }
			 catch (NumberFormatException e1) {
				 d1 = -Double.parseDouble(coef1[k].trim());
			 }
			 try {
	        	 i1=Integer.parseInt(degre1[j].trim());
			 }
			 catch(NumberFormatException e1) {
				 aux = degre1[j].trim();
	    		 aux=aux.substring(0, aux.length() - 1);
	    		 i1=Integer.parseInt(aux);
			 }
		    	 monos1[k] = new Monomial(d1,i1);
	         }		 
		 for (int k=0; k<monos1.length; ++k)
				 polOne.MonoList.add(monos1[k]); 
			 
			 for(int k=0, j=1; k<coef2.length; ++k, ++j)
		        {/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 
				 try {
		        	d2 = Double.parseDouble(coef2[k].trim());
				 }
				 catch(NumberFormatException e1) {
					d2 = -Double.parseDouble(coef2[k].trim()); 
				 }
				 try {
					 i2=Integer.parseInt(degre2[j].trim()); 
				 }
				 catch(NumberFormatException e1) {
					 aux = degre2[j].trim();
		    		 aux=aux.substring(0, aux.length() - 1);
		    		 i2=Integer.parseInt(aux);
				 }		        	 
			    	 monos2[k] = new Monomial(d2,i2);
		         }			 
			 for (int k=0; k<monos2.length; ++k)
					 polTwo.MonoList.add(monos2[k]); 						
			myPoli=Operations.addPoly(polOne,polTwo);			 
			result.setText(Operations.getPoliString(myPoli));
		 }
		}		 
		//SUBSTRACTION		 
	 else if (e.getSource()==subpol) {
		 if(checkPoliFormat(pol1.getText())==false || checkPoliFormat(pol2.getText())==false) {
			 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
		 }
		 else if(pol1.getText().equals("") || pol2.getText().equals("")) {
			 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
		 }
		 else
		 {		 
		 for(int k=0, j=1; k<coef1.length; ++k, ++j)
	        {/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 
			 try {
				 d1 = Double.parseDouble(coef1[k].trim());
			 }
			 catch(NumberFormatException e1) {
				 d1 = -Double.parseDouble(coef1[k].trim());
			 }
			 try {
				 i1=Integer.parseInt(degre1[j].trim());
			 }
			 catch(NumberFormatException e1) {
				 aux = degre1[j].trim();
	    		 aux=aux.substring(0, aux.length() - 1);
	    		 i1=Integer.parseInt(aux);
			 }			 
		    	 monos1[k] = new Monomial(d1,i1);
	         }
		 
		 for (int k=0; k<monos1.length; ++k)
				 polOne.MonoList.add(monos1[k]); 
			 
			 for(int k=0, j=1; k<coef2.length; ++k, ++j)
		        {/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 
				 try {
					 d2 = Double.parseDouble(coef2[k].trim());
				 }
				 catch (NumberFormatException e1) {
					 d2 = -Double.parseDouble(coef2[k].trim());
				 }
				 try {
					 i2=Integer.parseInt(degre2[j].trim()); 
				 }
				 catch(NumberFormatException e1) {
					 aux = degre2[j].trim();
		    		 aux=aux.substring(0, aux.length() - 1);
		    		 i2=Integer.parseInt(aux);
				 }		        	
			    	 monos2[k] = new Monomial(d2,i2);
		         }			 
			 for (int k=0; k<monos2.length; ++k)
					 polTwo.MonoList.add(monos2[k]); 
			 			
			myPoli=Operations.subPoly(polOne,polTwo);			 
			result.setText(Operations.getPoliString(myPoli));
		}
	 }		
	//MULTIPLICATION
	 else if (e.getSource()==multipol) {
		 
		 if(checkPoliFormat(pol1.getText())==false || checkPoliFormat(pol2.getText())==false ) {
			 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
		 }
		 else if(pol1.getText().equals("") || pol2.getText().equals("")) {
			 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
		 }
		 else
		 {		 
		 for(int k=0, j=1; k<coef1.length; ++k, ++j)
	        {/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 
			 try {
	        	d1 = Double.parseDouble(coef1[k].trim());
			 }
			 catch(NumberFormatException e1) {
				 d1 = -Double.parseDouble(coef1[k].trim());
			 }
			 try {
				 i1=Integer.parseInt(degre1[j].trim()); 
			 }
			 catch(NumberFormatException e1) {
				 aux = degre1[j].trim();
	    		 aux=aux.substring(0, aux.length() - 1);
	    		 i1=Integer.parseInt(aux);
			 }
		    	 monos1[k] = new Monomial(d1,i1);
	         }
		 
		 for (int k=0; k<monos1.length; ++k)
				 polOne.MonoList.add(monos1[k]); 
			 
			 for(int k=0, j=1; k<coef2.length; ++k, ++j)
		        {/**parsing the coefficient string into a double, and the degree string to an integer.@throw NumberFormatException**/	 
				 try {
					 d2 = Double.parseDouble(coef2[k].trim()); 
				 }
				 catch(NumberFormatException e1) {
					 d2 = -Double.parseDouble(coef2[k].trim());
				 }
				 try {
					 i2=Integer.parseInt(degre2[j].trim());
				 }
				 catch(NumberFormatException e1) {
					 aux = degre2[j].trim();
		    		 aux=aux.substring(0, aux.length() - 1);
		    		 i2=Integer.parseInt(aux);
				 }		        	
			    	monos2[k] = new Monomial(d2,i2);
		         }			 
			 for (int k=0; k<monos2.length; ++k)
					 polTwo.MonoList.add(monos2[k]); 
			 			
			myPoli=Operations.multiPoli(polOne,polTwo);			 
			result.setText(Operations.getPoliString(myPoli));
		}
	 }		 
	//DIVISION		 
	 else if (e.getSource()==dividepol) {
		 if(checkPoliFormat(pol1.getText())==false || checkPoliFormat(pol2.getText())==false ) {
			 jopt.showMessageDialog(null, err, "Error :(",JOptionPane.ERROR_MESSAGE);
		 }
		 else if(pol1.getText().equals("") || pol2.getText().equals("")) {
			 jopt.showMessageDialog(null, emptytext, "Input error", JOptionPane.ERROR_MESSAGE);
		 }
		 else
		 {		 
		 for(int k=0, j=1; k<coef1.length; ++k, ++j)
	        {/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 
			try {
				 d1 = Double.parseDouble(coef1[k].trim());
			 }
	        catch(NumberFormatException e1) {
	        	 d1 = -Double.parseDouble(coef1[k].trim());
	        }
			try {
				 i1=Integer.parseInt(degre1[j].trim());
			}
	        catch(NumberFormatException e1) {
	        	 aux = degre1[j].trim();
	    		 aux=aux.substring(0, aux.length() - 1);
	    		 i1=Integer.parseInt(aux);
	        }
		    	 monos1[k] = new Monomial(d1,i1);
	         }		 
		 for (int k=0; k<monos1.length; ++k)
				 polOne.MonoList.add(monos1[k]); 
			 
			 for(int k=0, j=1; k<coef2.length; ++k, ++j)
		        {/**parsing the coefficient string into a double, and the degree string to an integer,@throw NumberFormatException**/	 
				 try {
					 d2 = Double.parseDouble(coef2[k].trim());
				 }
				 catch(NumberFormatException e1) {
					 d2 = -Double.parseDouble(coef2[k].trim());
				 }
		        try {
		        	 i2=Integer.parseInt(degre2[j].trim());
		        }
		        catch(NumberFormatException e1) {
		        	aux = degre2[j].trim();
		    		aux=aux.substring(0, aux.length() - 1);
		    		i2=Integer.parseInt(aux);
		        }	        	
			    	 monos2[k] = new Monomial(d2,i2);
		         }			 
			 for (int k=0; k<monos2.length; ++k)
					 polTwo.MonoList.add(monos2[k]); 
			 
			//division with 0 
			if(polTwo.MonoList.size()==1 && polTwo.MonoList.get(0).coeff == 0 ) {
				JOptionPane.showMessageDialog(dialog, "You cannot divide by 0! :( ", "Error",JOptionPane.ERROR_MESSAGE);
			}
			else { //set textfield with the operation results
			polOne=Operations.removeInitialZeros(polOne);
			polTwo=Operations.removeInitialZeros(polTwo);
			try { /** perform division, @throw IndexOutOfBoundsException**/
			divPoli.MonoList=Operations.dividePoli(polOne,polTwo).MonoList;	
			divPoli.remainder = Operations.dividePoli(polOne, polTwo).remainder;			
			myPoli.MonoList = divPoli.MonoList;			
			result.setText(Operations.getPoliString(myPoli));
			myRest.MonoList = divPoli.remainder;			
			rest.setText(Operations.getPoliString(myRest));
			}
			catch(IndexOutOfBoundsException e1) {
				JOptionPane.showMessageDialog(dialog, "some error occured", "Error :'(", JOptionPane.ERROR_MESSAGE);
			}
			} //end of 0 detection	
		 }//end of input validation
		}//end of source analysing 	
	} //end of actionperformed	
	
	//REGEX	FOR INPUT POLYNOMIAL FORMAT VALIDATION
	public static Boolean checkPoliFormat(String s) {

	 /**checks if a string matches the polynomial pattern good for our project**/

		Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
		Matcher matcher = pattern.matcher(s);		
		while (matcher.find()) {
		    if(Operations.checkMonoFormat(matcher.group(1))==false)
		    	return false;
		}
		return true;
	}
	
}



	 		 
	 


	

	



