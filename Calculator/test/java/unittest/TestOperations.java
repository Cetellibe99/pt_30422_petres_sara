package unittest;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import Controller.Operations;
import Model.Monomial;
import Model.PoliWithRemainder;
import Model.Polynomial;

/**@author Petres Sara
 * Testing the arithmetic operations functions for individual randomly taken examples*/

public class TestOperations {
	static Polynomial pol1=new Polynomial(); 
	static Polynomial pol2 = new Polynomial(); 
	static Monomial mon11 = new Monomial(1,3);
	static Monomial mon12  = new Monomial(-1,0); 
	static Monomial mon21 = new Monomial(1,1);
	static Monomial mon22 = new Monomial(-1,0);	

	@Test
	public void testAdd() {
	//x^3-1+x-1=x^3+x-2		
		Polynomial sum = new Polynomial();
		Polynomial expected = new Polynomial();
		
		pol1.setElement(mon11); pol1.setElement(mon12); 
		pol2.setElement(mon21); pol2.setElement(mon22);
		
		Monomial exp = new Monomial(1,3);
		Monomial exp2 = new Monomial(1,1);
		Monomial exp3 = new Monomial(-2,0);
		expected.setElement(exp); expected.setElement(exp2);expected.setElement(exp3); 
		
		Polynomial temp1 = new Polynomial(); Polynomial temp2 = new Polynomial();
		temp1 = pol1; temp2 = pol2;
		sum=Operations.addPoly(temp1, temp2);
		
		/**the assertEquals function activates the JUnit truth validation procedure. It will be forced
		 * to be true when the poliEquals(polynomial) object equality detection function returns true
		 * and false when it isnt. The two polynomials couldn't have been the parameters of the assertEquals() function
		 * since they are special objects for which I created a personalized equality detector method 
		 */
		
		
		if (expected.poliEquals(sum)) 
		{
			assertEquals(1, 1);
		}
		else
			assertEquals(2,1);		
	}
	
	@Test
	public void testSub() {
		//x^3-1-(x-1)=x^3-x

		Polynomial dif = new Polynomial();
		Polynomial expected = new Polynomial();		
		Polynomial temp3 = new Polynomial();
		Polynomial temp4 = new Polynomial();
		temp3 = pol1; temp4 =  pol2;
				
		Monomial exp = new Monomial(1,3);
		Monomial exp2 = new Monomial(-1,1);
		Monomial zero = new Monomial(0,0);
		
		expected.setElement(exp); expected.setElement(exp2); expected.setElement(zero);								
		dif=Operations.subPoly(temp3, temp4);

		if (expected.poliEquals(dif))
		{
			assertEquals(1, 1);
		}
		else
			assertEquals(2,1);	
	}
	
	@Test
	public void testIntegrate() {	
		//integral (x^3-1) =0.25x^4-1x^1 		
		Polynomial res = new Polynomial();
		Polynomial expected = new Polynomial();
		Polynomial temp5 = pol1;	
		Monomial exp = new Monomial(0.25,4);
		Monomial exp2 = new Monomial(-1,1);
		
		expected.setElement(exp); expected.setElement(exp2); //expected.setElement(zero);						
		res=Operations.integratePoly(temp5);

		if (expected.poliEquals(res))
		{
			assertEquals(1, 1);
		}
		else
			assertEquals(2,1);	
	}
	
	@Test
	public void testDifferentiate() {
	// (2x^8-3.5x^2+9x^1)' = 16x^7-7x+9
		Polynomial difpoli = new Polynomial();
		Monomial md1 = new Monomial(2,8);
		Monomial md2 = new Monomial(-3.5,2);
		Monomial md3 = new Monomial(9,1);
		Polynomial expdif = new Polynomial();
		Monomial ed1 = new Monomial(16,7);
		Monomial ed2 = new Monomial(-7,1);
		Monomial ed3 = new Monomial(9,0);
		difpoli.setElement(md1);
		difpoli.setElement(md2);
		difpoli.setElement(md3);		
		expdif.setElement(ed1);
		expdif.setElement(ed2);
		expdif.setElement(ed3);
		Polynomial diffresult = new Polynomial();
		diffresult = Operations.derivatePoly(difpoli);
		
		if (diffresult.poliEquals(expdif))
		{
			assertEquals(1, 1);
		}
		else
			assertEquals(2,1);	
	}
	
	@Test
	public void testMultiply() {
		//(x^2-1)*(x^2) = x^4-x^2
		
		Polynomial mp1 = new Polynomial();
		Polynomial mp2 = new Polynomial();
		Polynomial multi = new Polynomial();
		Polynomial expmulti = new Polynomial();
		Monomial m1 = new Monomial(1,2);
		Monomial m2 = new Monomial(-1,0);
		Monomial em1 = new Monomial(1,4);
		Monomial em2 = new Monomial(-1,2);
		
		mp1.setElement(m1);
		mp1.setElement(m2);
		mp2.setElement(m1);
		expmulti.setElement(em1);
		expmulti.setElement(em2);
		multi = Operations.multiPoli(mp1, mp2);
		
		if (expmulti.MonoList.get(0).equals(multi.MonoList.get(0))) {
		}

		if (expmulti.poliEquals(multi))
		{
			assertEquals(1, 1);
		}
		else
			assertEquals(2,1);	
	}
	@Test
	public void testDivision() {
		//(x^3-1)/(x-1)=x^2+x+1
		
		Polynomial divp1 = new Polynomial();
		Polynomial divp2 = new Polynomial();
		PoliWithRemainder divresult = new PoliWithRemainder();
		
		PoliWithRemainder expdiv = new PoliWithRemainder();
		Monomial mdiv1 = new Monomial(1,3);
		Monomial mdiv2 = new Monomial(1,1);
		Monomial mdiv3 = new Monomial(-1,0);
		Monomial ediv1 = new Monomial(1,2);
		Monomial ediv2 = new Monomial(1,0);
		Monomial zero = new Monomial(0,0);
		
		divp1.setElement(mdiv1);
		divp1.setElement(mdiv3);
		divp2.setElement(mdiv2);
		divp2.setElement(mdiv3);
		expdiv.MonoList.add(ediv1);
		expdiv.MonoList.add(mdiv2);
		expdiv.MonoList.add(ediv2);
		expdiv.remainder.add(zero);
		
		divresult.MonoList=Operations.dividePoli(divp1,divp2).MonoList;
		divresult.remainder=Operations.dividePoli(divp1, divp2).remainder;	
		if (divresult.poliWithRemainderEquals(expdiv))
		{
			assertEquals(1, 1);
		}
		else
			assertEquals(2,1);	
	}


}
